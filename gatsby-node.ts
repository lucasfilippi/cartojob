//https://gist.github.com/JohnAlbin/2fc05966624dffb20f4b06b4305280f9

// Because we used ts-node in gatsby-config.js, this file will automatically be
// imported by Gatsby instead of gatsby-node.js.

// Use the type definitions that are included with Gatsby.
import { GatsbyNode } from "gatsby";
import { resolve } from "path";
import {Tree, Category, allCategoriesType, allOrganizationsType, Organization, allDispatchFormType, DispatchForm} from "./src/utils/model"


type allPagesGraphql = {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    errors?: any;
    data?: {
        categories?: allCategoriesType,
        organizations?: allOrganizationsType;
        forms?: allDispatchFormType;
    };
}

export const createPages: GatsbyNode["createPages"] = async ({
    actions,
    graphql,
}) => {
    const { createPage } = actions;

    const allPages: allPagesGraphql = await graphql(`
    query MyQuery {
        categories: allAirtable(filter: {table: {eq: "Category"}, data: {Name: {ne: null}, HasDedicatedPage: {eq: true}}}) {
            nodes {
                id
                data {
                    Order
                    Name
                    OrgCount
                    HasDedicatedPage
                }
            }
        }
        organizations: allAirtable(filter: { table: { eq: "Organization" }, data: {Status: {eq: "Validé"}}} ) {
            nodes {
                id
                data {
                    Name
                }
            }
        }
        forms: allAirtable(filter: { table: { eq: "DispatchForm" }} ) {
            nodes {
                id
                data {
                    Name
                }
            }
        }
    }
    `)
    
    let categories = new Tree('/categories', allPages.data?.categories.nodes);
    categories.walk((node: Category): void => {
        if (node.hasDedicatedPage) {
            createPage({
                path: node.slug,
                component: resolve(`./src/templates/category.tsx`),
                context: {
                    categoryId: node.id,
                    airtableId: node.airtableId
                },
            })
        }
    });

    allPages.data?.organizations?.nodes.forEach(org => {

        if (null !== org.data.Name) {
            let organization = new Organization({id: org.id, name: org.data.Name})
            createPage({
                path: organization.slug,
                component: resolve(`./src/templates/organization.tsx`),
                context: {
                    organizationId: organization.id,
                }
            })
        }
    })


    allPages.data?.forms?.nodes.forEach(org => {
        if (null !== org.data.Name) {
            let form = new DispatchForm(org.id, org.data.Name)
            createPage({
                path: form.slug,
                component: resolve(`./src/templates/form.tsx`),
                context: {
                    formId: form.id,
                }
            })
        }
    })
};
