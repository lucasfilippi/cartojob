## 🚀 Quick start

1.  **This is a Gatsby site.**

    This project use [Gatsby](https://www.gatsbyjs.org) for its static content.
    The search feature use [Meilisearch](https://github.com/meilisearch/MeiliSearch).
    The production build process is based on gitlab-ci.

2.  **Requirements**

    - You need at least NodeJS >=14 because ["optional chaining" is used](https://stackoverflow.com/questions/59574047/how-to-use-optional-chaining-in-node-js-12)
    - Follow 4 first steps of [Gatsby tutorial](https://www.gatsbyjs.org/tutorial/part-zero/) until install Gatsby CLI `npm install -g gatsby-cli`
    - Generate an API KEY on your [Airtable account page](https://airtable.com/account) and ask for permissions to view the Airtable base

3.  **Installation**

    - Run `npm install`
    - Go to `plugins/gatsby-plugin-meilisearch/` and run `npm install` (as meilisearch plugin is not published yet)
    - Create a file named `.env.development` at root of the project with the following content (see following section for Meilisearch related configuration)

    Ask us if you want access to airtable


        AIRTABLE_API_KEY=YOUR_API_KEY
        AIRTABLE_BASE_ID=
        GATSBY_MEILISEARCH_HOST=http://127.0.0.1:7700/
        GATSBY_MEILISEARCH_ORG_IDX_NAME=
        GATSBY_MEILISEARCH_PUBLIC_API_KEY=
        MEILISEARCH_PRIVATE_API_KEY=

4.  **Search: Meilisearch**

    Search is handled by [meilisearch](https://docs.meilisearch.com/).
    A Gatsby plugin is started but not finished yet (see plugins/gatsby-plugin-meilisearch).
    It's main purpose is to index data at build time (exactly using the onPostBuild Gatsby API hook).

    To enable search, you have to run your own meilisearch image

    This project use [Meilisearch Authentication](https://docs.meilisearch.com/guides/advanced_guides/authentication.html)
    All you have to do is choosing a MEILI_MASTER_KEY and launch a docker image like this :

    `docker run -e "MEILI_MASTER_KEY=YOUR_KEY" -p 7700:7700 -v $(pwd)/data.ms:/data.ms getmeili/meilisearch`

    You can retrieve your public and private key with the [keys route](https://docs.meilisearch.com/references/keys.html#get-keys).
    You can complete .env.development config :

        GATSBY_MEILISEARCH_HOST=http://127.0.0.1:7700/
        GATSBY_MEILISEARCH_ORG_IDX_NAME=youchoose
        GATSBY_MEILISEARCH_PUBLIC_API_KEY=got_from_ms_key_route
        MEILISEARCH_PRIVATE_API_KEY=got_from_ms_key_route

    To index into Meilisearch, just run `gatsby build`

5) **Start dev server**

   - Run `npm start` or `npm run develop` or `gatsby develop`

6) **Main dependencies**

   - Data is stored on Airtable and fetched with [gatsby-source-airtable](https://www.npmjs.com/package/gatsby-source-airtable)
   - Styling is done with [tailwindcss](https://tailwindcss.com/)
   - Maps is served by Open Street Map server using [leafletjs](https://leafletjs.com/) and [gatsby-plugin-react-leaflet](https://www.gatsbyjs.com/plugins/gatsby-plugin-react-leaflet/)

7) **Troubleshooting**

   - `code: 'MODULE_NOT_FOUND'` You probably have an old version of Node. Try to upgrade Node.
   - `Error: ENOSPC: System limit for number of file watchers reached,` You hit a system limit. You may try this : `echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p` (Tested on Ubuntu)
   - Airtable API do not return field which are always null, this can troubleshoot GraphQL queries

8) **Ops**

   Every push on master branch trigger a gitlab-ci build.
   When the site builds, it indexes data on meilisearch and publish the result on Gitlab pages on the following url : https://shiftyourjob.org/

   The env variables for the build process are stored in gitlab CI/CD settings > Variables
