// @ts-ignore
import React, {useEffect, useMemo, useRef} from "react"
import {MapContainer, TileLayer} from "react-leaflet";
import MarkerClusterGroup from 'react-leaflet-markercluster';
import {Coordinates, MapOrganization} from "../../utils/model";
import {OrganizationMarker} from "./OrganizationMarker";
import {useSearchContext} from "../SearchContextProvider";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faInfo} from "@fortawesome/free-solid-svg-icons";
import {Link} from "gatsby";
import {formatQueryParameters} from "../../utils/HistoryUtils";
//https://www.npmjs.com/package/react-leaflet-markercluster
type OrganizationsMapProps = {
    mapOrganizations: MapOrganization[]
}


function getBounds(coordinates: Coordinates[]): [[number, number], [number, number]] {
    const minCoordinates = coordinates.reduce((previousMinCoordinates, currentCoordinates) => {
        return {
            latitude: Math.min(previousMinCoordinates.latitude, currentCoordinates.latitude),
            longitude: Math.min(previousMinCoordinates.longitude, currentCoordinates.longitude)
        }
    }, {
        latitude: 90, longitude: 90
    })

    const maxCoordinates = coordinates.reduce((previousMaxCoordinates, currentCoordinates) => {
        return {
            latitude: Math.max(previousMaxCoordinates.latitude, currentCoordinates.latitude),
            longitude: Math.max(previousMaxCoordinates.longitude, currentCoordinates.longitude)
        }
    }, {
        latitude: -90, longitude: -90
    })
    return [[minCoordinates.latitude, minCoordinates.longitude], [maxCoordinates.latitude, maxCoordinates.longitude]]
}

function flatten<T>(arrayToFlatten: T[][]): T[] {
    return arrayToFlatten.reduce((accumulatedEntities, currentEntities) => [
        ...accumulatedEntities,
        ...currentEntities
    ], [])
}

const emptyCities: string[] = []

//This supposes there is a 1-1 mapping between officeCities <-> coordinates.
function getFilteredCoordinates(cities: string[], coordinates: Coordinates[], citiesFacets: string[]) {
    if (citiesFacets.length === 0) {
        return coordinates
    }
    const lowerCasedCitiesFacets = citiesFacets.map(cityFacet => cityFacet.toLowerCase())
    const cityIndexesToKeep = cities
        .map((city, index) => {
            if (lowerCasedCitiesFacets.includes(city.toLowerCase())) {
                return index
            }
            return -1
        })
        .filter(cityIndexToKeep => cityIndexToKeep !== -1)

    return coordinates.filter((_, index) => cityIndexesToKeep.includes(index))
}

export const FULL_COUNTRY_ORGANIZATION_CITY = "TOUTE LA FRANCE (FR)";

function isFullCountryOrganization(organizationCities: string[]) {
    return organizationCities.includes(FULL_COUNTRY_ORGANIZATION_CITY);
}

const OrganizationsMap = ({mapOrganizations}: OrganizationsMapProps) => {
    const mapRef = useRef(null)
    useEffect(() => {
        if (mapRef && mapRef.current && mapRef.current.leafletElement) {
            console.log("invalidateSize")
            mapRef.current.leafletElement.invalidateSize();
        }
    });
    const {searchFilters: {facets: {fullLocations: citiesFacets = emptyCities, ...otherFacets}, query}} = useSearchContext();
    const positionedOrganizations = useMemo(() => {
            return mapOrganizations
                .filter(({fullCoordinates}) => fullCoordinates.length > 0)
                .filter(({fullLocations: organizationCities}) => {
                    return !isFullCountryOrganization(organizationCities)
                })
                .map(mapOrganization => {
                    const coordonatesToKeep = getFilteredCoordinates(mapOrganization.fullLocations, mapOrganization.fullCoordinates, citiesFacets)
                    return ({
                        ...mapOrganization,
                        coordinates: coordonatesToKeep
                    });
                });
        }
        , [mapOrganizations, citiesFacets]);
    const bounds = useMemo(() => getBounds(flatten(positionedOrganizations.map(({coordinates}) => coordinates))), [positionedOrganizations]);


    if (typeof window === "undefined") {
        return null;
    }
    const linkToFullCountryOrganizations = `/search/?${formatQueryParameters({fullLocations: FULL_COUNTRY_ORGANIZATION_CITY, query, ...otherFacets})}`;
    return (
        <div className="bg-beige w-full h-full flex flex-col">
            <MapContainer ref={mapRef} bounds={bounds} center={[46.71109, 1.7191036]} zoom={3} maxZoom={14} className="w-full h-full z-0 mb-3">
                <TileLayer
                    url="http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                />
                <MarkerClusterGroup>
                    {positionedOrganizations
                        .map(mapOrganization => mapOrganization.coordinates
                            .map(coordinatesUnit => <OrganizationMarker
                                key={`${mapOrganization.id}-${coordinatesUnit.latitude}-${coordinatesUnit.longitude}`}
                                mapOrganization={mapOrganization}
                                coordinates={coordinatesUnit}/>
                            ))
                    }
                </MarkerClusterGroup>
            </MapContainer>
            <div className="mx-2 lg:mx-0 whitespace-pre-wrap">
                <FontAwesomeIcon
                    className="mr-2"
                    size="lg"
                    icon={faInfo}
                />
                Les implantations des acteurs nationaux ne sont pas représentées sur la carte. Pour en connaître la
                liste, <Link to={linkToFullCountryOrganizations}
                             className="custom-underline">cliquez ici</Link>.
            </div>

        </div>
    )

}

export default OrganizationsMap
