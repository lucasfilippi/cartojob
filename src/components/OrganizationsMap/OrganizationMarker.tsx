import React, {useMemo} from "react";
import {Coordinates, MapOrganization, Organization} from "../../utils/model";
import {Marker, Popup} from "react-leaflet";
import { Link } from "gatsby";
import slug from "slug"


type OrganizationMarkerProps = {
    mapOrganization: MapOrganization,
    coordinates: Coordinates
}

const OrganizationMarker = ({mapOrganization, coordinates}: OrganizationMarkerProps) => {
    const position = useMemo(() =>([coordinates.latitude, coordinates.longitude]), [coordinates.latitude, coordinates.longitude]);
    return <Marker position={position}>
        <Popup>
            <Link to={`/organizations/${slug(mapOrganization.name)}`}>{mapOrganization.name}</Link>
        </Popup>
    </Marker>
}

export {OrganizationMarker};
