import {MeilisearchPluginIndexOptions} from "./plugins/gatsby-plugin-meilisearch";
require("dotenv").config({
  path: `./.env.${process.env.NODE_ENV}`,
})
import queries from "./src/utils/meilisearch";

const RequiredEnv = [
  `AIRTABLE_BASE_ID`, `AIRTABLE_API_KEY`, `GATSBY_MEILISEARCH_HOST`
]

const missingEnv = RequiredEnv.filter(key => !process.env[key])

// Fail fast if any of the required ENV variables are missing
if (missingEnv.length) {
  throw new Error(`
    The following variable(s) are missing from .env.${process.env.NODE_ENV}:
    ${missingEnv.join(`, `)}
    Open .env.sample to learn how to fix this.
  `)
}

const gatsbyPluginMeilisearchOptions: MeilisearchPluginIndexOptions = {
    mode: "INDEX",
    host: process.env.GATSBY_MEILISEARCH_HOST,
    apiKey: process.env.MEILISEARCH_PRIVATE_API_KEY,
    queries,
};


module.exports = {
  // pathPrefix: `/carto-jobs-climat`,
  siteMetadata: {
    title: `SHIFT YOUR JOB - Concilier Emploi & Climat`,
    description: `Où travailler pour contribuer à la transition carbone ? Explorez les opportunités dans les secteurs contribuant à la transition et engagez-vous professionnellement pour le développement durable`,
    author: `les Shifters`,
    organizationAddFormUrl: `http://fake.com`,
    contactFormUrl: `https://airtable.com/shrTMKflEgoqDmSgV`,
    siteUrl: `https://shiftyourjob.org`,
  },
  plugins: [
    `gatsby-transformer-remark`,
    `gatsby-plugin-typescript`,
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-sitemap`,
    {
      resolve: 'gatsby-plugin-react-leaflet',
      options: {
        linkStyles: true // (default: true) Enable/disable loading stylesheets via CDN
      }
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-plugin-postcss`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#FFFFFF`,
        theme_color: `#1A3066`,
        display: `minimal-ui`,
        icon: `src/images/logo.svg`, // This path is relative to the root of the site.
      },
    },
    //Plugin page : https://www.npmjs.com/package/gatsby-source-airtable
    {
      resolve: `gatsby-source-airtable`,
      options: {
        apiKey: process.env.AIRTABLE_API_KEY,
        tables: [
          {
            baseId: process.env.AIRTABLE_BASE_ID,
            tableName: "Category",
            tableView: `Main`,
            mapping: { 'Description': 'text/markdown',
              'Highlights': 'text/markdown',
              'CategoryPicto': 'fileNode'},
            // tableLinks: [`Parent`],
          },
          {
            baseId: process.env.AIRTABLE_BASE_ID,
            tableName: "Organization",
            tableLinks: [`Categories`, `Lever`, `Trainings`, `Professions`, `Tags`, `City`, `Headquarter`],
            tableView: `Main`,
            mapping: { 'ActivityDescription': 'text/markdown'}
          },
          {
            baseId: process.env.AIRTABLE_BASE_ID,
            tableName: "Lever",
            mapping: { 'Picto': 'fileNode', 'Label': 'text/markdown'}
          },
          {
            baseId: process.env.AIRTABLE_BASE_ID,
            tableName: "DispatchForm",
            mapping: { 'Picto': 'fileNode'}
          },
          {
            baseId: process.env.AIRTABLE_BASE_ID,
            tableName: "CartoFiles",
            mapping: { 'PDF': 'fileNode', 'Notes': 'text/markdown'}
          },
          {
            baseId: process.env.AIRTABLE_BASE_ID,
            tableName: "Glossary",
            tableView: "Site view",
            mapping: { 'Notes': 'text/markdown'}
          },
          {
            baseId: process.env.AIRTABLE_BASE_ID,
            tableName: "Partner",
            mapping: {'Description': 'text/markdown', 'Picto': 'fileNode'}
          },
          {
            baseId: process.env.AIRTABLE_BASE_ID,
            tableName: "Training"
          },
          {
            baseId: process.env.AIRTABLE_BASE_ID,
            tableName: "Profession"
          },
          {
            baseId: process.env.AIRTABLE_BASE_ID,
            tableName: "Tag"
          },
          {
            baseId: process.env.AIRTABLE_BASE_ID,
            tableName: "City",
            tableView: `Prod`
          }
        ],
      },
    },
    {
      resolve: `gatsby-plugin-meilisearch`,
      options: gatsbyPluginMeilisearchOptions,
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
    // https://www.gatsbyjs.com/plugins/gatsby-plugin-google-tagmanager/
    {
      resolve: "gatsby-plugin-google-tagmanager",
      options: {
        id: "GTM-NDWJW7P",
        includeInDevelopment: true,
      },
    },
  ],
}
