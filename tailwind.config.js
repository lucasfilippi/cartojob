module.exports = {
  theme: {
   
    screens: {
      'sm': '640px',
      'md': '768px',
      'lg': '1024px',
      'xl': '1280px',
      '2xl': '1600px',
      '3xl': '1900px',
      '4xl': '2400px',
      'portrait': {'raw': '(orientation: portrait)'},
    },
     placeholderColor: {
    },
    fontSize: {
      '2xs': '.6rem',
      '11px': '.6875rem',
      'xs': '.75rem',
      'sm': '.875rem',
      'base': '0.925rem',
      'lg': '1.125rem',
      'xl': '1.25rem',
      '2xl': '1.5rem',
      '3xl': '1.875rem',
      '4xl': '2.25rem',
      '5xl': '3rem',
      '6xl': '4rem',
      '7xl': '5rem',
      '8xl': '7rem',
      '9xl': '8.625rem',
      '10xl': '9rem',
    },
    maxHeight: {
      "12": "3em",
    },
    boxShadow: {
      xs: '0 0 0 1px rgba(0, 0, 0, 0.05)',
      sm: '0 1px 2px 0 rgba(0, 0, 0, 0.05)',
      default: '0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06)',
      md: '0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -1px rgba(0, 0, 0, 0.06)',
      lg: '0 10px 15px -3px rgba(0, 0, 0, 0.1), 0 4px 6px -2px rgba(0, 0, 0, 0.05)',
      xl: '0px 2px 16px rgba(0, 0, 0, 0.15)',
      '2xl': '0 25px 50px -12px rgba(0, 0, 0, 0.25)',
      '3xl': '0 35px 60px -15px rgba(0, 0, 0, 0.3)',
      inner: 'inset 0 2px 4px 0 rgba(0, 0, 0, 0.06)',
      outline: '0 0 0 3px rgba(66, 153, 225, 0.5)',
      focus: '0 0 0 3px rgba(66, 153, 225, 0.5)',
      none: 'none',
    },
    extend: {
      fontFamily: {
        serif: `Georgia, "Times New Roman", serif`,
        sans: `Source Sans Pro, Roboto, system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI", "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"`,
       // mono: `"Roboto Mono", Robot, Menlo, Monaco, Consolas, "Liberation Mono", "Courier New", monospace`,
         display: ['Georgia', "Times New Roman", 'serif'],
         body: ['Georgia', "Times New Roman", 'serif']
       },
      maxHeight: {
        'half': "25vh"
      },
      maxWidth: {
        xs: '20rem',
        sm: '24rem',
        md: '28rem',
        lg: '32rem',
        xl: '36rem',
        '2xl': '42rem',
        '3xl': '48rem',
        '4xl': '56rem',
        '5xl': '64rem',
        '6xl': '72rem',
        '7xl': '80rem',
      },
      zIndex: {
        "1000": 1000,
        "5000": 5000
      },
      inset: {
        "1/2": "50%",
        "-1": "-1rem",
        "-0.5":"-0.5rem"
      },
      colors: {
        gray: {
          '100': '#f5f5f5',
          '200': '#eeeeee',
          '300': '#e0e0e0',
          '400': '#bdbdbd',
          '500': '#9e9e9e',
          '600': '#757575',
          '700': '#48576F',
          '800': '#424242',
          '900': '#212121',
        },
        'beige': '#FCFAF4',
        'primary': '#1A3066',
        'secondary': '#66D3B9',
        'shift': '#00508C'
      },
      gridTemplateRows: theme => {
        const rowHeight = theme("height")["56"] || "18rem";
        return ({
          "organizations": `repeat(4, minmax(0,${rowHeight}))`,
          "large-organizations": `repeat(2, minMax(0,${rowHeight}))`
        });
      }
    },
  },
  variants: {},
  plugins: [
     require('autoprefixer'),
  ],
}
