import {MeilisearchIndexQuery} from "./index";
import MeiliSearch from "meilisearch";
import {sortBy} from "../../src/utils/commonUtils";
import * as CommonUtils from "../../src/utils/commonUtils";
import report from "gatsby-cli/lib/reporter";
import {chunk} from 'lodash';

const identity = (obj) => obj;


export async function indexToMeilisearch(queries: MeilisearchIndexQuery[], activity: any, graphql: any, meilisearchInstance: MeiliSearch) {
    setStatus(activity, `${queries.length} queries to index to Meilisearch.`);
    const indexingJobs = queries.map(async (
        meilisearchIndexQuery: MeilisearchIndexQuery,
        queryIndex
    ) => {
        try {
            await doIndexToMeilisearch(meilisearchIndexQuery, activity, graphql, meilisearchInstance, queryIndex)
        } catch (exception) {
            setStatus(activity, `Failed to handle index ${meilisearchIndexQuery.indexName}. Let's delete it.`)
            await meilisearchInstance.getIndex(meilisearchIndexQuery.indexName).deleteIndex()
            throw exception
        }
    });
    await Promise.all(indexingJobs);
}

export async function purgeMeilisearchIndexes(numberOfIndexesToKeep: number, activity: any, meilisearchInstance: MeiliSearch) {
    setStatus(activity, `Starting to purge meilisearch indexes. Keeping last ${numberOfIndexesToKeep} indexes.`)
    const indexResponses = await meilisearchInstance.listIndexes();
    if (indexResponses.length <= numberOfIndexesToKeep) {
        setStatus(activity, `Meilisearch does not contain more than ${numberOfIndexesToKeep} indexes, keeping them (${indexResponses.length}) all.`)
        return;
    }
    const sortedIndexes = [...indexResponses.sort(sortBy("createdAt"))];
    const indexUidsToDelete = sortedIndexes.slice(0, indexResponses.length - numberOfIndexesToKeep).map(({uid}) => uid)
    await Promise.all(indexUidsToDelete.map((indexUidToDelete) => {
        setStatus(activity, `Deleting index ${indexUidToDelete}.`)
        return meilisearchInstance
            .getIndex(indexUidToDelete)
            .deleteIndex()
    }))
    setStatus(activity, `Purged meilisearch indexes. Keeping last ${numberOfIndexesToKeep} indexes.`)
}

async function doIndexToMeilisearch({indexName, query, settings, sort, transformer, options}: MeilisearchIndexQuery, activity: any, graphql: any, meilisearchInstance: MeiliSearch, queryIndex: number) {
    const index = await meilisearchInstance.getOrCreateIndex(indexName, options);
    await index.updateSettings(settings);
    setStatus(activity, `Query #${queryIndex}: executing query`);
    const result = await graphql(query);
    if (result.errors) {
        report.panic(`Failed to index to Meilisearch`, result.errors);
    }
    const objects = await transformer(result);
    /* We sort here because Meilisearch only provides ranking solution based on numerical fields. (https://docs.meilisearch.com/guides/advanced_guides/settings.html#ranking-rules)
       We could compute a lexiographical number based on the field but it seems over killed and complex for what we want to achieve here.
     */
    if (sort) {
        objects.sort(CommonUtils.sortBy(sort.key, sort.order))
    }
    setStatus(
        activity,
        `Query ${queryIndex}: graphql resulted in ${Object.keys(objects).length} records`
    );

    let chunks = chunk(objects, 100)
    
    for (const chunk of chunks) {

        const {updateId} = await index.addDocuments(chunk as any)
        const indexUpdate = await index.waitForPendingUpdate(updateId, {
            timeOutMs: 15000,
            intervalMs: 50,
        })
        setStatus(activity, `Update ${updateId}: status ${indexUpdate.status}, processed in ${indexUpdate.duration}`);
    }
}

export function setStatus(activity, status) {
    if (activity && activity.setStatus) {
        activity.setStatus(status);
    } else {
        console.log('Meilisearch:', status);
    }
}
