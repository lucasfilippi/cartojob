import {MeilisearchPluginOptions} from "./index";
import MeiliSearch from "meilisearch";
import {indexToMeilisearch, purgeMeilisearchIndexes} from "./gatsby-utils";
import report from "gatsby-cli/lib/reporter";


const onPostBuild = async (
    {graphql},
    meilisearchPluginOptions
        : MeilisearchPluginOptions
) => {
    try {
        const activity = report.activityTimer(`Meilisearch plugin started in mode: ${meilisearchPluginOptions.mode}`);
        activity.start();
        const {apiKey, host} = meilisearchPluginOptions
        const meilisearchInstance = new MeiliSearch({host, apiKey});
        switch (meilisearchPluginOptions.mode) {
            case "INDEX":
                await indexToMeilisearch(meilisearchPluginOptions.queries, activity, graphql, meilisearchInstance)
                break;
            case "PURGE_INDEXES":
                await purgeMeilisearchIndexes(meilisearchPluginOptions.numberOfIndexesToKeep, activity, meilisearchInstance)
                break;
        }
        activity.end();
    } catch (exception) {
        report.panic(`Failed to run gastby-meilisearch plugin: ${exception}`)
    }
}

exports.onPostBuild = onPostBuild;

